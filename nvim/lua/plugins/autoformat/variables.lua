vim.g.formatdef_gci_local = '"gci print -s standard -s default -s localmodule"'
vim.g.formatters_go = { 'gofmt_1', 'goimports', 'gci_local', 'gofmt_2', 'gofumpt' }   
vim.g.run_all_formatters_go = 1
