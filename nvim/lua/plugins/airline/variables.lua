
vim.g.airline_theme = 'bubblegum'
vim.g.airline_powerline_fonts = 1
vim.g.airline_highlighting_cache = 1
vim.g[ 'airline#extensions#branch#format' ] = 2
vim.g[ 'airline#extensions#hunks#enabled' ] = 0
vim.g.airline_section_b = '%{airline#util#wrap(airline#extensions#branch#get_head(),120)}'
vim.g.airline_section_y = ''
