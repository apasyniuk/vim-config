
--Variable.g({
  --nvim_tree_show_icons = {
    --['git'] = 1,
    --['folders'] = 1,
    --['files'] = 1,
    --['folder_arrows'] = 0,
  --},
--})
require'nvim-tree'.setup {
  disable_netrw       = true,
  hijack_netrw        = true,
  hijack_directories   = {
    enable = true,
    auto_open = false,
  },
  hijack_cursor       = false,
  update_cwd          = false,
  update_focused_file = {
    enable      = false,
    update_cwd  = false,
    ignore_list = {}
  },
  system_open = {
    cmd  = nil,
    args = {}
  },
}

