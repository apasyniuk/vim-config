-- Install packer if it is not installed yet
if vim.fn.isdirectory(vim.fn.expand("~/.local/share/nvim/site/pack/packer/start/packer.nvim")) == 0 then
    os.execute("git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim")
end

require('plugins.install-plugins')

require('plugins.telescope')
require('plugins.nvim-treesitter')
require('plugins.nvim-tree')
require('plugins.lsp')
require('plugins.colors')
require('plugins.airline')
require('plugins.quick-scope')
require('plugins.submode')
require('plugins.fugitive')
require('plugins.autoformat')

--require('plugins.fterm-nvim')
--require('plugins.undotree')
--require('plugins.vimspector')
