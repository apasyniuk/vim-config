local lsp = require("lsp-zero")

lsp.preset({
    name = 'recommended',
    set_lsp_keymaps = false,
    suggest_lsp_servers = false,
    sign_icons = {
        error = 'E',
        warn = 'W',
        hint = 'H',
        info = 'I'
    }
})

lsp.ensure_installed({
    'rust_analyzer',
    'gopls',
    'luau_lsp',
})
lsp.nvim_workspace()

local cmp = require('cmp')
local cmp_select = { behavior = cmp.SelectBehavior.Select }
local cmp_mappings = lsp.defaults.cmp_mappings({
    ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
    ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
    ['<C-y>'] = cmp.mapping.confirm({ select = true }),
    ["<C-Space>"] = cmp.mapping.complete(),
})

cmp_mappings['<Tab>'] = nil
cmp_mappings['<S-Tab>'] = nil

lsp.setup_nvim_cmp({
    mapping = cmp_mappings,
    sources = {
        { name = 'path' },
        { name = 'nvim_lsp' },
        { name = 'buffer',                 keyword_length = 3 },
        { name = 'luasnip',                keyword_length = 2 },
        { name = 'nvim_lsp_signature_help' }
    }
})

cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = 'buffer' },
    }
})

cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        {
            name = 'path'
        }
    }, {
        {
            name = 'cmdline',
        }
    })
})


lsp.on_attach(function(client, bufnr)
    local opts = { buffer = bufnr, remap = false }
    local telescope = require('telescope.builtin')

    vim.keymap.set('n', '<Leader>ld', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', '<Leader>lh', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', '<Leader>li', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<Leader>lt', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<Leader>ln', vim.lsp.buf.rename, opts)
    vim.keymap.set('n', '<Leader>la', vim.lsp.buf.code_action, opts)
    vim.keymap.set('v', '<Leader>la', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', '<Leader>lr', telescope.lsp_references, {})
    vim.keymap.set('n', '<Leader>le', vim.diagnostic.setloclist, opts)
    vim.keymap.set('n', '<Leader>lf', vim.lsp.buf.format, opts)
end)

require('lspconfig').gopls.setup({
    settings = {
        gopls = {
            gofumpt = true,
            analyses = {
                unusedparams = true,
            },
            staticcheck = true
        }
    }
})

lsp.setup()

vim.diagnostic.config({
    virtual_text = true
})
