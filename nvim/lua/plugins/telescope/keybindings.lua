

local builtin = require('telescope.builtin')

vim.keymap.set('n', '<leader>ts',
    function()
        builtin.grep_string({ use_regex = true, search = vim.fn.input("Grep For >")})
    end
, { noremap = true, silent = true }  )

vim.keymap.set('n', '<leader>tw',
    function()
        builtin.grep_string({ search = vim.fn.expand("<cword>") })
    end
, { noremap = true, silent = true }  )

vim.keymap.set('n', '<leader>tb', builtin.buffers, { noremap = true, silent = true }  )
vim.keymap.set('n', '<leader>tf', builtin.find_files, { noremap = true, silent = true }  )
vim.keymap.set('n', '<leader>tl', builtin.lsp_dynamic_workspace_symbols, { noremap = true, silent = true }  )
