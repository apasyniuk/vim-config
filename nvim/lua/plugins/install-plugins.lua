require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'

    use 'vim-airline/vim-airline'
    use 'vim-airline/vim-airline-themes'
    use 'kyazdani42/nvim-web-devicons'
    use "ellisonleao/gruvbox.nvim"

    use {
        'nvim-telescope/telescope.nvim',

        requires = { { 'nvim-lua/plenary.nvim' } }
    }
    use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
    use 'nvim-telescope/telescope-symbols.nvim'
    use 'nvim-lua/plenary.nvim'

    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
            ts_update()
        end
    }
    use("nvim-treesitter/nvim-treesitter-context");


    use 'nvim-tree/nvim-tree.lua'
    use 'mbbill/undotree'
    use 'tpope/vim-fugitive'
    use 'airblade/vim-gitgutter'

    use 'tpope/vim-surround'
    use 'preservim/nerdcommenter'

    use {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v2.x',
        requires = {
            -- LSP Support
            { 'neovim/nvim-lspconfig' },
            { 'williamboman/mason.nvim' },
            { 'williamboman/mason-lspconfig.nvim' },

            -- Autocompletion
            { 'hrsh7th/nvim-cmp' },
            { 'hrsh7th/cmp-buffer' },
            { 'hrsh7th/cmp-path' },
            { 'hrsh7th/cmp-cmdline' },
            { 'hrsh7th/cmp-vsnip' },
            { 'hrsh7th/cmp-nvim-lsp-signature-help' },
            { 'saadparwaiz1/cmp_luasnip' },
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'hrsh7th/cmp-nvim-lua' },

            -- Snippets
            { 'L3MON4D3/LuaSnip' },
            { 'rafamadriz/friendly-snippets' },
        }
    }


    use({
        "glepnir/lspsaga.nvim",
        branch = "main",
        requires = {
            { "nvim-tree/nvim-web-devicons" },
            --Please make sure you install markdown and markdown_inline parser
            { "nvim-treesitter/nvim-treesitter" }

        },
    })
    use("github/copilot.vim")

    use 'unblevable/quick-scope'

    use 'tpope/vim-repeat'

    use 'vim-scripts/argtextobj.vim'
    use 'christoomey/vim-tmux-navigator'
    use 'NLKNguyen/papercolor-theme'

    use 'preservim/vimux'
    use 'benmills/vimux-golang'
    use 'sk1418/QFGrep'
    use 'mileszs/ack.vim'

    ----------------------------
    use 'RishabhRD/popfix'
    use 'RishabhRD/nvim-cheat.sh'
    ----------------------------
    --
    use 'Tuxdude/mark.vim'
    use 'kamykn/spelunker.vim'
    use 'AndrewRadev/linediff.vim'

    use 'kana/vim-submode'

    use 'chiel92/vim-autoformat'
    use 'sebdah/vim-delve'
    use({
        "olimorris/codecompanion.nvim",
        requires = {
            "nvim-lua/plenary.nvim",
            "nvim-treesitter/nvim-treesitter",
            "hrsh7th/nvim-cmp",              -- Optional: For activating slash commands and variables in the chat buffer
            "nvim-telescope/telescope.nvim", -- Optional: For working with files with slash commands
            "stevearc/dressing.nvim"         -- Optional: Improves the default Neovim UI
        }
    })
    use {
      "someone-stole-my-name/yaml-companion.nvim",
      requires = {
          { "neovim/nvim-lspconfig" },
          { "nvim-lua/plenary.nvim" },
          { "nvim-telescope/telescope.nvim" },
      },
      config = function()
        require("telescope").load_extension("yaml_schema")
      end,
    }
end)

require("lspsaga").setup({
    symbol_in_winbar = {
        enable = false,
    },
    lightbulb = {
        enable = false,
    },
})
require("codecompanion").setup({
    adapters = {
        openai = function()
            return require("codecompanion.adapters").extend("openai", {
                env = {
                    api_key = "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",
                },
            })
        end,
    },
})
local cfg = require("yaml-companion").setup({
  -- Add any options here, or leave empty to use the default settings
  -- lspconfig = {
  --   cmd = {"yaml-language-server"}
  -- },
})
require("lspconfig")["yamlls"].setup(cfg)
