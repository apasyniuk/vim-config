require('nvim-treesitter.configs').setup({
  ensure_installed = {"go", "c", "lua", "gomod", "html", "json", "make", "vim", "yaml", "comment" },
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  indent = {
    enable = true,
  },
  --autotag = {
    --enable = true,
  --},
})
