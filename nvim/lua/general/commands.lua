local autocmd = vim.api.nvim_create_autocmd

autocmd("TextYankPost", {
        callback = function()
            vim.highlight.on_yank({timeout = 300})
        end
})


autocmd('InsertEnter' , {
        callback = function()
            vim.api.nvim_set_hl(0, 'CursorLine', {bg="#282828" ,fg=nil})
        end
})
autocmd('InsertLeave' , {
        callback = function()
            vim.api.nvim_set_hl(0, 'CursorLine', {bg="#3C3836"  ,fg=nil})
        end
})

autocmd('FileType' , {
        pattern = 'go',
        callback = function()
            vim.api.nvim_buf_set_option(0, 'expandtab', false)
        end
})

--autocmd('FileType' , {
        --pattern = 'vim',
        --callback = function()
            --vim.api.nvim_buf_set_option(0, 'foldmethod', 'marker')
        --end
--})

autocmd('FileType' , {
        pattern = 'yaml',
        callback = function()
            vim.api.nvim_buf_set_option(0, 'tabstop', 2)
            vim.api.nvim_buf_set_option(0, 'shiftwidth', 2)
        end
})

autocmd('FileType' , {
        pattern = 'git',
        callback = function()
            vim.api.nvim_win_set_option(0, 'foldmethod', 'syntax')
        end
})

autocmd('BufWrite' , {
        pattern = '*.go',
        callback = function()
            vim.cmd('Autoformat')
        end
})
